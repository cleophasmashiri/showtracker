angular.module('MyApp').
controller('DetailController', ['$scope', '$rootScope', '$routeParams', 'ShowService', 'SubscriptionService', 
    function($scope, $rootScope, $routeParams, ShowService, SubscriptionService){
        ShowService.get({_id: $routeParams.id}, function(show){
            $scope.show = show;

            $scope.isSubscribed  = function(){
                return $scope.show.subscribers.indexOf($rootScope.currentUser.id);
            };

            $scope.subscribe = function(){
                Subscription.subscribe(show).success(function(){
                    $scope.show.subscribers.push($rootScope.currentUser._id);
                });
            };

            $scope.unsubscribe = function(){
                SubscriptionService.unsubscribe(show).success(function(){
                    var index = $scope.show.subscribers.indexOf($rootScope.currentUser._id);
                    $scope.show.subscribers.splice(index, 1);
                });
            };

            $scope.nextEpisode = show.episodes.filter(function(episode){
                return new Date(episode.firstAired) > new Date();
            });
        });
}]); 