angular.module('MyApp')
  .factory('SubscriptionService', ['$http', function($http) {
    return {
        subscribe: function(show, user){
         return $http.post('/api/subscribe', {showid: show._id});
        }, 
      
        unsubscribe: function(show, user){
        return $http.post('/api/unsucribe',{showid: show._id});                                   
        }
     };
  }]);